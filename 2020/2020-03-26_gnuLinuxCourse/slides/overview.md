# Overview

0. Prerequisites
1. LCSB's  Credentials Management System : LUMS
2. Effective Shell Usage
   1. Basics
     1. At the Gates
	 2. Documentation Systems
	 3. Linux Filesystem Structure
	 4. Environment
   2. Effective Use
     1. History
	 2. Text Manipulation Tools
