# File Explorer: Effective Command Line Editing

```console
$ # Move up the history list until 'mkdir linux-practical-01'.
$ # Move cursor using word motions 'Ctrl' + 'Left', or 
$ # 'Ctrl' + 'Right' around the command to get the feel for word motions.
$ # Move cursor using begin ('Ctrl' + 'A') and end ('Ctrl' + 'E') 
$ # motions to the beginning and the end of the line.
$ mkdir linux-practical-02    # Based on the previous command + motions.
$ cd lin    # TAB to complete, then TAB two times to list possible endings.
$ cd linux-practical-02    # Based on the previous completion.
$ # Move up the history list to mkdir subdir-01
$ mkdir subdir-02
$ # Move up the history to the touch subdir-01/a ... command.
$ # Replace 1 with 2 using 'Ctrl' + 'A' and word motions.
$ touch subdir-02/a subdir-02/b subdir-02/c
$ history 10 # Lists last ten commands.
$ cd
```
<!-- * Motions -->
<!--   - `Left` and `Right` arrow move one character left, or right -->
<!--   - `Ctrl` + `Left`, or `Right` move left, or right for one word -->
<!--   - `Ctrl` + `A`, or `E` move to the beginning, or the end of the line -->
<!--   - `Up` and `Down` move through history entries -->
<!--   - `TAB` completes -->
