# File Explorer: Effective Command Line Editing (Concepts)

* Moving using arrow keys is tedious

* Retyping commands that contain a typo is even more tedious

* *Readline* facility of the bash shell offers quicker navigation
  - Move by a word left and right using `Ctrl`+`Left`, or `Right`
  - Move to a beginning, or the end of the line using `Ctrl`+`A` and `Ctrl`+`E`
  - Delete an entire word using `Alt`+`D`
  - Paste a deleted word with `Ctrl`+`Y`

* Reuse previous commands!
  - The shell remembers what you type
  - Move to a previous line in history using `Up` and `Down` arrows
