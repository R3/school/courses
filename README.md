# Presentation slides

This repository contains all presentations related to R3.

Detailed instructions are here: https://gitlab.lcsb.uni.lu/R3/outreach/templates/presentation
