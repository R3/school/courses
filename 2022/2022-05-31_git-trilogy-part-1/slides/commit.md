# New term: <font color="color:red">commit</font>

* Your repository (your file) is in a certain state.
* Every change moves the repository to a new state.
* Every change added to the sequence of changes is called **commit**.
* Every commit has:
  * content - what?
  * a message - why?
  * an author - who?
  * a timestamp - when?
  * unique identifier - tracking number

<div style="position:absolute;left:60%;top:1em">
<img src="slides/img/commit_history-diagram.png" class="as-is" height="800px"/>
</div>

<div class="fragment">
Gitlab:
<div style="position:absolute">
<img src="slides/img/commit_GUI_1.png" class="as-is" height="350px"/>
</div>
</div>



# New term: <font color="color:red">commit</font>

<img src="slides/img/icon-live-demo.png" height="100px">

1. Notice last commits that modified files/folders
<!-- .element: class="fragment" data-fragment-index="1" -->
1. Review history of your repository
<!-- .element: class="fragment" data-fragment-index="2" -->
1. See changes in the last/second last/third last commit.
<!-- .element: class="fragment" data-fragment-index="3" -->


<div class="fragment fade-in-then-out" style="position:absolute;left:50%;top:1em" data-fragment-index="1">
    <img src="slides/img/commit_GUI_main-last-commit.png" class="as-is" height="500px"/>
</div>

<div class="fragment fade-in-then-out" style="position:absolute;left:50%;top:1em" data-fragment-index="2">
    <img src="slides/img/commit_GUI_main-history.png" class="as-is" height="500px"/>
</div>

<div class="fragment fade-in-then-out" style="position:absolute;left:50%;top:4em" data-fragment-index="3">
<img src="slides/img/commit_GUI_click-on-commit.png" class="as-is" height="75px"/>
</div>
<div class="fragment fade-in-then-out" data-fragment-index="3">
<img src="slides/img/commit_changes.png" class="as-is" width="90%"/>
</div>
