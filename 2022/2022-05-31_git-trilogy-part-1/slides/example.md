# Complete example

Task: add your name to the list of attendees in the practice repository.

- Repository: [https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages](https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages)
- Development Branch: `develop`
- Website: [https://r3.pages.uni.lu/school/git/basic-practice-pages](https://r3.pages.uni.lu/school/git/basic-practice-pages)


<img src="slides/img/icon-live-demo.png" height="100px">
