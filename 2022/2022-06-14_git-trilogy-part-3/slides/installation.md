# The terminal (shell)

**macOS users:**

> Start the Terminal from your `/Applications` directoy.

![bulb](slides/img/bulb.png) Install iTerm2: [https://www.iterm2.com](https://www.iterm2.com)

<br>

**Windows users:**

> Install Git Bash: [https://git-scm.com/download/win](https://git-scm.com/download/win)

<br>

**Linux users:**

> Launch default terminal.<br>
![bulb](slides/img/bulb.png) Install Terminator: [https://launchpad.net/terminator](https://launchpad.net/terminator)



# Installation of `git`

<img src="slides/img/github_app.png" class="as-is" height="200" />

**macOS**

> Install *Xcode Command Line Tools*

<br>

**Windows**

> Install Git Bash: <br>`https://git-scm.com/download/win`

<br>

**Linux (Ubuntu)**

```bash
$ sudo apt-get install git-all
```



# How to get started?

**macOS**

> Start the `Terminal` or `iTerm`.

<br>

**Windows**

> Start `Git Bash`.

<br>

**Linux (Ubuntu)**

> Start the `Terminal` or `Terminator`.
