# Let's refresh our memories

<div class="fragment">

- What are the **5 essential commands**?

<div class="fragment">

- How can I update my fork locally?

<div class="fragment">

- What can I do to include the work from somebody else in my own work?

<div class="fragment">

- What if things go wrong?



# References & Cheat sheet

[1]: Git Book: https://git-scm.com/book/en/v2

[2]: GitHub training services: https://services.github.com/training/

[3]: Cheat sheet: http://rogerdudler.github.io/git-guide



# Thank you.

<center><img src="slides/img/r3-training-logo.png" height="200px"></center>

Contact us if you need help:

<a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a>
