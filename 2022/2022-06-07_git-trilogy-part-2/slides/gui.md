# Visual Studio Code - Graphical User Interface (GUI)

<img src="slides/img/icon-live-demo.png" height="100px"> GUI for VS code

<center>
<img src="slides/img/VSCodeGUI.png" width="60%">
</center>



# Visual Studio Code - Graphical User Interface (GUI)


- **Activity Bar** - Located on the left side, this lets you switch between views and gives you additional context-specific indicators, like the number of changes when Git is enabled.
- **Side Bar** - Contains information to assist while working on your project.
- **Editor** - The main area to edit files. You can open as many editors as you like side by side vertically and horizontally.
- **Panels** - You can display different panels below the editor region for output or debug information, errors and warnings, or an integrated terminal.
- **Status Bar** - Information about the opened project and the files you edit.

Source: VS code documentation