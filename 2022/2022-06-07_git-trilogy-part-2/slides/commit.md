# Make changes and commit them

When you change a file, save them by clicking on `File > Save` or by hitting a shortcut (`CTRL + S` or `Command + S`).

![bulb](slides/img/bulb.png)  On the left side, observe the git icon.

<br>
To commit your changes:

1. Go to Source Control in Activity Bar
2. Stage all changes you want to commit (press +)
3. Describe your changes in commit message
4. Commit your changes (press "tick")

<img src="slides/img/icon-live-demo.png" height="100px">